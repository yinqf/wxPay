[QQ群]：470414533 <br/>
[QQ]: 842324724 <br/>
[email] : 842324724@qq.com <br/>
有问题或者疑问，请加群咨询。一起学习。<br/>

[项目简要]：<br/>
1、本项目提供最新版本微信JSAPI支付的完整demo，包括使用JSSDK支付、使用微信浏览器自带的对象发起支付 这两种方式支付。<br/>

2、代码注释非常详细，也举出了两者的不同和需要注意的地方。<br/>

3、意在为需要这方面教程的朋友提供实例，节省时间和精力，学习到更多东西，共同进步。<br/>

4、同时也解决微信支付的教程在网络资源上的严重短缺和部分教程代码过期的问题。<br/>

5、JSAPI的两种支付方式，项目中进行了分别开发，每一种支付，对应一个jsp支付页面和JAVA类，方便大家学习。<br/>

6、可在eclipse开发工具中导入项目，直接部署至tomcat运行。 <br/><br/>
    
[jar文件的说明]：<br/>
项目中使用的一些jar文件，在网上比较难以寻找，请到项目根目录下的jar目录中引用到本地仓库，如若jar不全，请联系我索要。QQ:842324724 <br/><br/>
    
    
[微信公众平台 - 微信支付 - 开发配置 - 服务器相关配置说明]：<br/>
    <br/>
    1、项目发布到我个人的服务器，服务器访问URL为http://zhoubang85.com，端口为80。 
    <br/>
    <br/>
    2、由于我的服务器域名没有经过ICP备案，所以，只能选择【支付测试】栏目下的配置来进行支付开发，同时将自己的微信号添加到【测试白名单】，不然会报没有权限。 
    <br/>
    <br/>
    3、测试授权目录  ：  http://zhoubang85.com/ ，由于支付页面chooseWXPay.jsp和WeixinJSBridge.jsp都是在项目的根目录下，所以，测试授权目录就配置为http://zhoubang85.com/，而不是http://zhoubang85.com/chooseWXPay.jsp/ 或者 http://zhoubang85.com/WeixinJSBridge.jsp/ 
    <br/>
    <br/>
    4、对于像springmvc或者struts 相关的action请求进入支付页面，测试授权目录应该如何写呢？简单举个例子，如下： <br/>
        假如支付页面 chooseWXPay.jsp 是在/WEB-INF/views/目录下，而进入此页面的action请求路径为 http://zhoubang85.com/wxpay/chooseWXPay ，那么，测试授权目录将配置为 http://zhoubang85.com/wxpay/，而不是 http://zhoubang85.com/wxpay/chooseWXPay/ 
    <br/>
    <br/>

[项目运行之前的一些必要条件配置]：

1、请在settings.properties中配置你的appid、mchid商户号、apikey支付密钥。<br/>
            appid: 微信公众平台 - 开发者中心 - 配置项 - 开发者ID - AppID(应用ID)<br/>
            mchid商户号：微信公众平台 - 微信支付 - 商户信息 - 基本数据 - 商户号<br/>
            apikey支付密钥：微信商户平台 - API安全 - 密钥设置<br/><br/>
2、请在wxinf.properties中配置你的appid、secret。<br/>
            appid： (同上)<br/>
            secret: 微信公众平台 - 开发者中心 - 配置项 - 开发者ID - AppSecret(应用密钥)<br/><br/>
            
[注意]：
com.zb.controller包下面的2个java类，代码中的静态全局变量openId的值，目前我是写成我自己的openid，由于只是测试微信支付的demo，所以对于openid的获取，我就不多做笔画。到时候你们自己实现获取openid，这里不影响实际支付测试。

<br/>
[补充]：
有的朋友由于不熟悉maven，对于jar的获取比较头疼，所以在这里，我将所有用到的jar进行了打包放在了项目的根目录jar目录下，请自行放置到项目中即可。
<br/>
项目中补充了5个功能，分别是：
关闭订单、查询订单、查询退款、下载对账单、申请退款。初学者可以借鉴一下代码，实现这3个功能的操作。其实代码都差不多的。很简单。

<br/>
[问题咨询]:<br/>
如果在支付过程中遇到问题，可以联系我，我将为你解答。<br/>
QQ : 842324724 <br/>
Email : 842324724@qq.com <br/>
